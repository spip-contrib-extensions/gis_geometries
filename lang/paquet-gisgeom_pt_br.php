<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-gisgeom?lang_cible=pt_br
// ** ne pas modifier le fichier **

return [

	// G
	'gisgeom_description' => 'Este plugin adiciona o suporte a formas geométricas ao GIS. Ele usa as funções espaciais do MySQL disponíveis a partir da versão 4.1.',
	'gisgeom_slogan' => 'Suporte a formas geométricas no GIS',
];
